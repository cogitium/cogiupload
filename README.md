# CogiMail - A simple-featured upload/rename and resize(later, not yet) class for PHP #

## Class Features

- A simple code for uploading file from PHP
- Upload, rename and eventualy resize (later, not yet) the file
- Error messages in english only
- Generate exception if a trouble appears
- Compatible with PHP 5.0 and later

## Why you might need it

Because it is really boring to develop this ;)

## License

Coming soon.

## Installation & loading

At the moment, you just have to copy the class file in the folder you want.

### What's included

Within the download you'll find the following directories and files. You'll see something like this:

```
cogiupload/
├── bat/
│   ├── checkstyle.bat
│   ├── phpdoc.bat
├── class/
│   └── CogiUpload.class.js
├── doc/
│   ├── ...
│   └── index.html
├── psr/
│   ├── ...
│   └── index.html
└── test/
    ├── testCogiUpload.php
    ├── image1.jpg
    ├── image2.gif
    ├── image3.png
    ├── image4.jpeg
    ├── fichier1.pdf
    ├── fichier2.doc
    └── uploads
	    ├── ...
	    └── ...
```

## A Simple Example

```php
<?php
require_once("../class/CogiUpload.class.php");

if (isset($_FILES["myfile"])) {

    var_dump($_FILES);

    try {

        /**
         * Initialize the email and the sender detail
         */
        $upload = new CogiUpload($_FILES["myfile"], 
                                    array("jpg", "gif"), 
                                    1000000);

        /**
         * Use the setters
         */
        $upload->setPathToUpload('uploads');
        //$upload->setNameForFile('mynewfile');

        /**
         * Process the file
         */
        $url = $upload->processUpload();
        ?>
        
        <h2>Upload done</h2>
        <div>
        <?php
        echo "<p>Very good, the upload is done !</p>";
        echo "<p>URL : " . $url. "</p>";
        $upload->debug();
        ?>
        </div>
        
        <?php

    } catch(Exception $e) {

        echo '<p>Exception : ' . $e->getMessage() . '</p>';
        $upload->debug();
    }
}
?>
<h2>Please select a file to upload</h2>
<form method="post" action="#" enctype="multipart/form-data">
    <label>File to upload : </label>
    <input type="file" name="myfile">
    <br>
    <input type="submit" name="" value="Upload the file">
</form>
```

That's it. You should now be ready to use CogiUpload ! 

## Localization

English only at this time.

## Documentation

The complete documentation is in the doc folder.

## Tests

There is a test page in the test folder to try the component.

## Contributing

An idea or a comment, please email to [Philippe](mailto:phgiraud@cogitium.com)

## Changelog

See [changelog](changelog.md).

## History

- At the beginning, CogiUpload was a procedural function in a PHP library.
- Then, this function became a Class.
- Now, CogiUpload is in a BitBucket repository.
- With Readme, and a PhpDocumentor folder.
- And Cherry on the Cake, there is a PSR folder to see the quality of the code ;-)