<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Test page for CogiUpload Class</title>
</head>

<body>
    <div class="container">
        <h2>Test page for CogiUpload</h2>
        <div>
            <?php
            require_once("../class/CogiUpload.class.php");

            if (isset($_FILES["myfile"])) {
    
                var_dump($_FILES);

                try {

                    /**
                     * Initialize the email and the sender detail
                     */
                    $upload = new CogiUpload($_FILES["myfile"], 
                                                array("jpg", "gif"), 
                                                1000000);

                    /**
                     * Use the setters
                     */
                    $upload->setPathToUpload('uploads');
                    //$upload->setNameForFile('mynewfile');

                    /**
                     * Process the file
                     */
                    $url = $upload->processUpload();
                    ?>
                    
                    <h2>Upload done</h2>
                    <div>
                    <?php
                    echo "<p>Very good, the upload is done !</p>";
                    echo "<p>URL : " . $url. "</p>";
                    $upload->debug();
                    ?>
                    </div>
                    
                    <?php

                } catch(Exception $e) {

                    echo '<p>Exception : ' . $e->getMessage() . '</p>';
                    $upload->debug();
                }
            }
            ?>
            <h2>Please select a file to upload</h2>
            <form method="post" action="#" enctype="multipart/form-data">
                <label>File to upload : </label>
                <input type="file" name="myfile">
                <br>
                <input type="submit" name="" value="Upload the file">
            </form>
        </div>
    </div>      
</body>
</html>
