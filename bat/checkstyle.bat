rem **************************************************************
rem ***** DEBUT PhpCheckstyle script
rem **************************************************************

rem ***** Suppression de l'ancien rapport
del ..\psr\*.*

rem ***** Lancement de l'analyse
php c:\wamp\www\Dropbox\tools2\phpcheckstyle\run.php --src ..\class --outdir ..\psr --format html,xml --lang fr-fr

rem **************************************************************
rem ***** FIN CheckStyle script
rem **************************************************************
pause
