rem **************************************************************
rem ***** DEBUT PhpDoc script
rem **************************************************************

rem ***** Suppression de l'ancien rapport
del ..\doc\*.*

rem ***** Lancement de l'analyse
php c:\wamp\www\Dropbox\tools2\phpDocumentor-2.9.0\bin\phpdoc -d ..\class -t ..\doc 

rem **************************************************************
rem ***** FIN PhpDoc script
rem **************************************************************
pause
